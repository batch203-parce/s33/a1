//console.log("Hello World");

	// GET Method and map method to return the title of every item
	fetch("https://jsonplaceholder.typicode.com/todos")
	.then(response => response.json())
	//.then(response => console.log(response))
	.then(json =>{json.map(post => console.log(post.title))});


	// GET Method that provide the title and status of a to do list item
	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then(response => response.json())
	.then(response => console.log(response));


	// POST Method that will add/create to do list item
	fetch("https://jsonplaceholder.typicode.com/todos",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "Created To Do List Item",
					completed: true,
					userId: 1
				})
			})
	.then(response => response.json())
	.then(json => console.log(json));


	// PUT Method that will update a to do list item
	fetch("https://jsonplaceholder.typicode.com/todos/1",
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "Updated To Do List Item",
					description: "Description of to do list item",
					status: "Pending",
					dateCompleted: "Pending",
					userId: 1
				})
			})
	.then(response => response.json())
	.then(json => console.log(json));


	// PATCH Method that will update a specific do list item property
	fetch("https://jsonplaceholder.typicode.com/todos/1",

			{
				method: "PATCH",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					status: "Completed",
					dateCompleted: "12/01/2022"
				})
			})
	.then(response => response.json())
	.then(json => console.log(json));


	// DELETE Method that will delete a list item
	fetch("https://jsonplaceholder.typicode.com/todos/1", 
		{
			method: "DELETE"
		})
	.then(response => response.json())
	.then(json => console.log(json));